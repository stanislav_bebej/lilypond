%% Multivoice   << {  } \\ {  } >> |
%% Legato       ( )

\version "2.14.2"

\header {
  title = "Preludium 1"
  composer = \markup { "Norbert" \concat { Kub \char ##x00E1 t } "Op.1.a." }
}

\relative c'' {
  \new PianoStaff <<

    % Prava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Andante

      << { e2.( d4) } \\ { g,4 gis a2 } >> |
      << { c2.( b4) } \\ { a4 as g2 } >> |
      << { c4( cis d dis) } \\ { e,2 d4 a' } >> |
      << { e'4( f g f) } \\ { a,2 g4 a } >> |
      << { d2( b) } \\ { a2 g4 f } >> |
      << { c'1 } \\ { e,1 } >> |
      \bar "|."
    }

    % Lava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Andante
      \clef "bass"

      << { c2 a } \\ { c,2( f) } >> |
      << { d'4 dis e d } \\ { fis,2( g) } >> |
      << { c4 bes a b } \\ { a4( g f2) } >> |
      << { c'1 } \\ { e,4( d e f) } >> |
      << { b4 f' e d } \\ { g,1~ } >> |
      << { c1 } \\ { <c, g'>1 } >> |
      \bar "|."
    }
  >>
}
