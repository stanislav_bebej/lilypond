%% Multivoice   << {  } \\ {  } >> |
%% Legato       ( )

\version "2.14.2"

\header {
 title = "Preludium 2"
 composer = \markup { "Norbert" \concat { Kub \char ##x00E1 t } "Op.1.a." }
}

\relative c' {
  \new PianoStaff <<

    % Prava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Lento

      << { e2( g4 d } \\ { c2 b } >> |
      << { c2 d4 dis) } \\ { c1 } >> |
      << { e2( a4 g } \\ { b,2 e } >> |
      << { f2 d4 dis) } \\ { d4 c b2~ } >> |
      << { e4( f e d } \\ { b1 } >> |
      << { c4 c' b a) } \\ { c,1~ } >> |
      << { g'2( f4 e } \\ { c1 } >> |
      << { g'4 f e d) } \\ { d2 a } >> |
      << { c2.( b4 } \\ { g2 a4 g } >> |
      << { c1) } \\ { g1 } >> |
      \bar "|."
    }

    % Lava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Lento
      \clef "bass"

      << { g2 f } \\ { c2( g } >> |
      << { e'2 f4 fis } \\ { a,2 as) } >> |
      << { g'2 bes } \\ { g,2( cis, } >> |
      << { a''2 f } \\ { d,2 g) } >> |
      << { e'1~ } \\ { gis,1( } >> |
      << { e'2 dis } \\ { a2 fis) } >> |
      << { e'2 f4 bes } \\ { g,4( bes a g } >> |
      << { a'2 g4 f } \\ { d,2 f) } >> |
      << { e'4 es d2 } \\ { g,1( } >> |
      << { e'1 } \\ { c,1) } >> |
      \bar "|."
    }
  >>
}
