%% Arpeggio     \arpeggio
%% Ottava       \ottava #1  \ottava #0
%% Fermata      \fermata
%% Staccato     -.
%% Legato       ( )
%% Accent       ->
%% Tenuto       --
%% Dynamics     \pp \< \!
%% Markup       ^\markup { \italic cantabile }
%% Fingering    -number

\version "2.14.2"

\header {
  title = "Barkarola"
  composer = "Jacques Offenbach (1819-1880)"
}

\score {
  \relative c'' {
    \new PianoStaff <<

      % Prava ruka
      \new Staff {
        \key d \major
        \time 6/8
        \tempo Allegretto

        % Riadok 1, strana 1
        fis16(\mf-1 cis' fis fis, cis' fis fis, d' fis fis, b fis' |
        fis, cis' fis fis, cis' fis fis, d' fis fis, b fis' |
        fis,\< cis' fis cis ais fis\!-1 cis\>-3 ais-2 fis-1 cis-3 ais-2 fis\!-1 |
        r4) r16 cis'''(\p-2 <fis, fis'>4.)\fermata |

        % Riadok 2
        a16(\p-1 e' a a, e' a a, f' a a, d a' |
        a, e' a a, e' a a, f' a a, d a' |
        a,\< e' a e cis a\! e\>-3 cis-2 a-1 e-3 cis a\! |
        r4) r16\ottava #1 e'''(\pp-2 <a, a'>4.)\fermata |

        % Riadok 3
        <a a'>8-.\f r <b b'>-. <b b'>-. r <a a'>-. |
        <a a'>16( cis e a-1 cis e a8)-.\ottava #0 r8 r |
        <a,,, a'>-.\p r <b b'>-. <b b'>-. r <a a'>-. |
        <a a'>16( cis e a-1 cis e a8)-. r8 r |
        <a,, cis e a>-.\f \arpeggio r <b b'>-. <b b'>-. r <a a'>-. |

        % Riadok 4
        <a cis e a>-. \arpeggio r <b b'>-. <b b'>-. r <a a'>-. |
        <a d fis a>-. \arpeggio r <b b'>-. <b b'>-. r <a a'>-. |
        <a cis g' a>-._\markup { \italic ritard. }\arpeggio r <b b'>-. <b b'>-. r <a a'>-. |
        \bar "||"
        \tempo Moderato
        <a d fis a>-.\arpeggio\pp r <b d fis b>-.\arpeggio <b d fis b>-.\arpeggio r <a d fis a>-.\arpeggio |
        <a d fis a>-.\arpeggio r <b d fis b>-.\arpeggio <b d fis b>-.\arpeggio r <a d fis a>-.\arpeggio |
        <d,-1 fis-2>4(^\markup { \italic cantabile } g8) g4( fis8) |

        % Riadok 5
        <cis fis>8( e g) g4( fis8) |
        <cis fis>8( e g) g4( fis8) |
        << { r4 <d' fis b>8-.\arpeggio\pp <d fis b>-.\arpeggio r <d fis a>-.\arpeggio } \\ { <d, fis>2. } >> |
        <d fis>4(\p g8) g4( fis8) |
        <cis fis>8( e g) g4( fis8) |
        <cis fis>8( e g) g4( fis8) |

        % Riadok 6, strana 2
        << { r4 <d' fis b>8-.\arpeggio\pp <d fis b>-.\arpeggio r <d fis a>-.\arpeggio } \\ { <d, fis>2. } >> |
        <fis-1 a-2>4(\p <fis b>8) <fis b>4(\< <a cis>8) |
        <a cis>4( <b d>8)\! <b d>4( <a cis>8) |
        << { <a cis>4( <g b>8)\> <g b>4( <fis a>8) } \\ { s2. } >> |
        << { r4 <d' fis b>8-.\arpeggio\pp\! <d fis b>-.\arpeggio r <d fis a>-.\arpeggio } \\ { <d, fis>2. } >> |
        <fis a>4(\p <fis b>8) <fis b>4(\< <a cis>8) |

        % Riadok 7
        <a cis>4( <b d>8)\! <b d>4( <a cis>8) |
        << { <a cis>4( <g b>8)\> <g b>4( <fis a>8) } \\ { s2. } >> |
        << { r4 <d' fis b>8-.\arpeggio\pp\! <d fis b>-.\arpeggio r <d fis a>-.\arpeggio } \\ { <d, fis>2. } >> |
        << { a'4(\mf\<-2 b8) b4( c8)\! } \\ { fis,2.-1 } >> |
        << { d'2.~ } \\ { fis,4(\<-2 g8)-1 g4( a8)\! } >> |
        << { d8 a(-3 b c b\>-4 a-3 } \\ { <fis a>4 r8 d( e-1 fis-2 } >> |

        % Riadok 8
        << { <g-1 b-4>4)\! } \\ { s4) } >> <d' g c>8-.\arpeggio\p <d g c>-.\arpeggio r <d g b>-.\arpeggio |
        << { b4(\mf\<-2 cis8) cis4( d8)\! } \\ { gis,2.-1 } >> |
        << { e'2.~ } \\ { gis,4(\<-2 a8)-1 a4( b8)\! } >> |
        << { e8 b(-3 cis d cis\>-4 b-3 } \\ { <gis b>4 r8 e( fis-1 gis-2 } >> |
        << { a4)\!-1 } \\ { s4) } >> <cis e b'>8-.\arpeggio\p <cis e b'>-.\arpeggio r <cis e a>-.\arpeggio |

        % Riadok 9
        << { a4.~(->\mf-3 a8\> cis b } \\ { <dis, fis>2. } >> |
        << { <e g a>4)\! } \\ { s4 } >> <cis' e b'>8-.\arpeggio\p <cis e b'>-.\arpeggio r <cis e a>-.\arpeggio |
        << { a4.~(->\mf\<-3 a8 cis b } \\ { <dis, fis>2. } >> |
        << { <e g a>4)\f\! } \\ { s4 } >> <cis' e b'>8\arpeggio\< <cis e b'>\arpeggio r <cis e a>\arpeggio\! |
        <cis e a>\arpeggio\> r <cis e b'>8\arpeggio <cis e b'>\arpeggio r <cis e a>\arpeggio\! |

        % Riadok 10
        << { fis,4(--\mp^\markup { \italic a tempo }-1 g8-- g4-- fis8-- } \\ { fis16 a d fis g, g' g, a d g fis, fis' } >> |
        << { fis,8-- e-- g-- g4-- fis8-- } \\ { fis16 fis' e, e' g, g' g, a cis g' fis, fis' } >> |
        << { fis,8-- e-- g-- g4-- fis8-- } \\ { fis16 fis' e, e' g, g' g, a cis g' fis, fis' } >> |

        % Riadok 11
        << { s2.) } \\ { <fis, fis'>16--\< a(-1 d fis-4 a-1 d\! fis\>-4 d a fis-4 d-2 a)\! } >> |
        << { fis4(-- g8-- g4-- fis8-- } \\ { fis16 a d fis g, g' g, a d g fis, fis' } >> |
        << { fis,8-- e-- g-- g4-- fis8-- } \\ { fis16 fis' e, e' g, g' g, a cis g' fis, fis' } >> |

        % Riadok 12, strana 3
        << { fis,8-- e-- g-- g4--\< fis8-- } \\ { fis16 fis' e, e' g, g' g, a cis g' fis, fis' } >> |
        << { a,4-- b8-- b4-- a8-- } \\ { a16 c dis a' b, b' b, c dis b' a, a' } >> |
        << { g,8)--\mf\! e(-- fis-- g-- a-- b-- } \\ { g16 g' e, e' fis, fis' g, g' a, a' b, b' } >> |

        % Riadok 13
        << { a,4--\< b8-- b4-- a8-- } \\ { a16 d fis a b, b' b, d fis b a, a' } >> |
        << { a,8-- a-- b-- cis-- d-- e-- } \\ { a,16 a' a, a' b, b' cis, cis' d, d' e, e' } >> |
        << { <fis, fis'>2.)\f\! } \\ { r8 dis,16( dis' e, e' fis, fis' g, g' a, a' } >> |
        << { s8 e-- d-- cis-- d-- b-- } \\ { <g g'>8) e'16( e' d, d' cis, cis' d, d' b, b' } >> |

        % Riadok 14
        << { a,16 a' fis-3 a e-2 a cis,-2 a' d,-2 a' b, b' } \\ { a,4.~-- a4 b8-- } >> |
        << { s2. } \\ { cis16->\> a' cis cis, g'-2 cis cis, fis-2 cis' cis,-- e-2 cis' } >> |
        << { s4 } \\ { <d, fis d'>4)\p\! } >> r8 << { a8(^\markup { \italic dolce }-3 d4) } \\ { <d,-1 fis-2>4. } >> |  % podla predlohy je osminova pomlcka jednohlasna
        %      << { s4. a,8( d4) } \\ { <d fis d'>4) r8 <d, fis>4. } >> |  % to iste, no spojene hlasy. Osminova pomlcka patri spodnemu hlasu
        << { a'8(-3 e'4~ e4.) } \\ { <cis,-1 g'-2>4. a8( g'4) } >> |

        % Riadok 15
        << { d'8(-3 fis4) a,8(-3 d4) } \\ { fis,4.-1 <d-1 fis-2>4. } >> |
        << { a'8(-3 e'4~ e4.) } \\ { <cis,-1 g'-2>4. a8( g'4) } >> |
        <fis-1 d'-2>8( a') <d, fis b>-.\arpeggio_\markup { \italic dimin. } <d fis b>-.\arpeggio r <d fis a>-.\arpeggio |
        <fis,-1 d'-3>8( fis') <a, d g>-.\arpeggio <a d g>-.\arpeggio r <a d fis>-.\arpeggio |
        <fis-1 a-2>8( d') <fis, a e'>-.\arpeggio <fis a e'>-.\arpeggio r <fis a d>-.\arpeggio |

        % Riadok 16
        <d-1 fis-2>8(\> a') <d, fis b>-.\arpeggio <d fis b>-.\arpeggio r <d fis a>-.\arpeggio |
        <a d fis-4>8-.\arpeggio r <a d g>-.\arpeggio <a d g>-.\arpeggio r <a d fis>-.\arpeggio |
        <fis a d>8-.\pp\! fis-.(-2 a-.-3 d-.-1 fis-. a-. |
        d8-.) fis,-.(_\markup { \italic smorzando }-2 a-.-3 d-.-1 fis-. a-. |
        d4-.) r8 <g,,, bes d>4--\pp r8 |
        <fis a d fis>2.\fermata |

        \bar "|."
      }

      % Lava ruka
      \new Staff {
        \key d \major
        \time 6/8
        \tempo Allegretto

        % Riadok 1, strana 1
        <fis'-5 cis'-2>4.( <b-3 d-1>4 <d,-5 b'-2>8 |
        <fis-4 cis'-1>4. <b-2 d-1>4 <d, b'>8 |
        <fis cis'>4.) r4 r8 |
        \clef "bass" <fis,, cis'>2.\fermata |

        % Riadok 2
        \clef "treble" <a''-5 e'-2>4.( <d-3 f-1>4 <f,-5 d'-2>8 |
        <a-4 e'-1>4. <d-2 f-1>4 <f, d'>8 |
        <a e'>4.) r4 r8 |
        \clef "bass" <a,, e'>2.\fermata |

        % Riadok 3
        \clef "treble" <fis'' a c dis>2.-> |
        <g a cis e>4.~-> <g a cis e>8 r r |
        \clef "bass" <fis, a c dis>2.-> |
        <g a cis e>4.~-> <g a cis e>8 r r |
        << { r4 r8 <a cis e>4.\arpeggio } \\ { <a, a'>2.-> } >> |

        % Riadok 4
        << { r4 r8 <a' cis e>4.\arpeggio } \\ { <g, g'>2.-> } >> |
        << { r4 r8 <a' d fis>4.\arpeggio } \\ { <fis, fis'>2.-> } >> |
        << { r4 r8 <a' cis g'>4.\arpeggio } \\ { <e, e'>2.-> } >> |
        \bar "||"
        \tempo Moderato
        <d d'>4 r8 <a'' d fis>4\arpeggio r8 |
        <d,, d'>4 r8 <a'' d fis>4\arpeggio r8 |
        d,,4 r8 <d' a' d>4\arpeggio r8 |

        % Riadok 5
        a4 r8 <e' g a cis>4\arpeggio r8 |
        a,4 r8 <e' g a cis>4\arpeggio r8 |
        <d, d'>4 r8 <fis' a d>4\arpeggio r8 |
        d,4 r8 <d' a' d>4\arpeggio r8 |
        a4 r8 <e' g a cis>4\arpeggio r8 |
        a,4 r8 <e' g a cis>4\arpeggio r8 |

        % Riadok 6, strana 2
        <d, d'>4 r8 <fis' a d>4\arpeggio r8 |
        d,4 r8 <fis' a d>4\arpeggio r8 |
        e,4 r8 <e' g a>4\arpeggio r8 |
        <a,, a'>4 r8 <e'' g a cis>4\arpeggio r8 |
        <d, d'>4 r8 <fis' a d>4\arpeggio r8 |
        d,4 r8 <fis' a d>4\arpeggio r8 |

        % Riadok 7
        e,4 r8 <e' g a>4\arpeggio r8 |
        <a,, a'>4 r8 <e'' g a cis>4\arpeggio r8 |
        <d, d'>4 r8 <fis' a d>4\arpeggio r8 |
        d,8 r d'-5 <fis a d>\arpeggio r d |
        d,8 r d' <a' c d>\arpeggio r d, |
        d,8 r d' <fis a c>\arpeggio r d |

        % Riadok 8
        g,8 r d' <g b d>\arpeggio r d |
        e,8 r e' <gis b e>\arpeggio r e |
        e,8 r e' <b' d e>\arpeggio r e, |
        e,8 r e' <gis b d>\arpeggio r e |
        a,8 r e'(-5 a-3 cis e) |

        % Riadok 9
        a,,,8 r a'-5 <dis fis a>\arpeggio r a |
        a,8 r e''(-5 a-3 cis e) |
        a,,,8 r a' <dis fis a>\arpeggio r a |
        <a, a'>8 cis'(-3 e-2 a-1 cis-3 e |
        g8 e cis a-1 g e) |

        % Riadok 10
        <d, d'>4 r8 <d' a' d>4\arpeggio r8 |
        <a, a'>4 r8 <e'' a cis>4\arpeggio r8 |
        <a,, a'>4 r8 <e'' a cis>4\arpeggio r8 |

        % Riadok 11
        <d, d'>4 r8 <fis' a d>4\arpeggio r8 |
        <d, d'>4 r8 <d' a' d>4\arpeggio r8 |
        <a, a'>4 r8 <e'' a cis>4\arpeggio r8 |

        % Riadok 12, strana 3
        <a,, a'>4 r8 <e'' a cis>4\arpeggio r8 |
        <dis, dis'>4 r8 <dis' fis c'>4\arpeggio r8 |
        <e, e'>4 r8 <e' g b>4\arpeggio r8 |

        % Riadok 13
        <a,, a'>4 r8 <fis'' a d>4\arpeggio r8 |
        <a,, a'>4 r8 <e'' g a cis>4\arpeggio r8 |
        <dis, dis'>4 r8 <dis' fis c'>4\arpeggio r8 |
        <e, e'>4 r8 <g'b e>4\arpeggio r8 |

        % Riadok 14
        <a,, a'>4 r8 <fis'' a d>4\arpeggio r8 |
        <a,, a'>4 r8 <g'' a cis>4\arpeggio r8 |
        d,8(-5 a'-3 d-2 fis-1 d a) |
        d,8( a'-3 cis-2 g'-1 cis, a) |

        % Riadok 15
        d,8( a' d fis d a) |
        d,8( a' cis g' cis, a) |
        d,8 r a'-5 <d fis a>\arpeggio r a |
        d,8 r a' <d a'>\arpeggio r a |
        d,8 r a' <d fis a>\arpeggio r a |

        % Riadok 16
        d,8 r a' <d fis a>\arpeggio r a |
        d,8 r a'-2 d r a |
        d,4 d'8-.(-5 fis-.-3 a-.-2 d-.-1 |
        fis-.)-2 a,-.(-5 d-. \clef "treble" fis-.-2 a-.-1 d-.-2 |
        fis4-.)-2 r8 \clef "bass" <g,,, d'>4-- r8 |
        << { <d d'>2.\fermata } \\ { r4 r8\pp\ottava #-1 d,4.\fermata\ottava #0 } >> |

        \bar "|."
      }
    >>
  }
}
