%% Multivoice   << {  } \\ {  } >> |
%% Legato       ( )

\version "2.14.2"

\header {
 title = "Preludium 3"
 composer = \markup { "Norbert" \concat { Kub \char ##x00E1 t } "Op.1.a." }
}

\relative c' {
  \new PianoStaff <<

    % Prava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Moderato

      << { e4( f g c) } \\ { c,4 d e2 } >> |
      << { b'4( a g e) } \\ { g4 f d c } >> |
      << { f4( g e c } \\ { d2 c } >> |
      << { d4) g( b d } \\ { b,4 r r2 } >> |
      << { f''2) cis( } \\ { r4 d,( e a } >> |
      << { d2. b4) } \\ { fis2) g4 gis } >> |
      << { a2( cis } \\ { a2 g } >> |
      << { d'4 c b2) } \\ { a2. g4 } >> |
      << { c4( b a2~ } \\ { e2 f4 g } >> |
      << { a2 g4) f } \\ { f4 e d2 } >> |
      << { e4( f d e } \\ { c2. b4 } >> |
      << { c1) } \\ { g1 } >> |
      \bar "|."
    }

    % Lava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Moderato
      \clef "bass"

      << { c1 } \\ { c2.( a4) } >> |
      << { e'4 c b c } \\ { e,4( f g a) } >> |
      << { a4 g2.~ } \\ { d4( b c e } >> |
      << { g4 r r2 } \\ { g4) r r2 } >> |
      << { r1 } \\ { r1 } >> |
      << { r4 a( b e } \\ { r1 } >> |
      << { f2) e } \\ { r4 d,( e a } >> |
      << { d1 } \\ { f,2 g) } >> |
      << { c2. cis4 } \\ { a4( g f e } >> |
      << { d'4 c b as } \\ { d,1) } >> |
      << { g4 f fis f } \\ { e4( a, as g } >> |
      << { e'1 } \\ { c,1) } >> |
      \bar "|."
    }
  >>
}
