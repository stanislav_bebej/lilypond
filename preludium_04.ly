%% Multivoice   << {  } \\ {  } >> |
%% Legato       ( )

\version "2.14.2"

\header {
 title = "Preludium 4"
 composer = \markup { "Norbert" \concat { Kub \char ##x00E1 t } "Op.1.a." }
}

\relative c' {
  \new PianoStaff <<

    % Prava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Andante

      << { r1 } \\ { c1~ } >> |
      << { r4 f( a g8 f } \\ { c2. d4~ } >> |
      << { g4 fis8 g a b c d) } \\ { d,2 r } >> |
      << { e'1~ } \\ { r4 g,8 a bes4 a8 g } >> |
      << { e'4 d8( c b2) } \\ { f2~ f8 a g f } >> |
      << { c'2~( c8 d e4) } \\ { e,2 g } >> |
      << { f'8( e d c~ c b c d) } \\ { f,2 fis } >> |
      << { e'4( d c2~ } \\ { g4 f f2~ } >> |
      << { c'1~ } \\ { f,4 e8 d e4 f8 d } >> |
      << { c'1) } \\ { e,1 } >> |
      \bar "|."
    }

    % Lava ruka
    \new Staff {
      \key c \major
      \time 4/4
      \tempo Andante
      \clef "bass"

      << { r4 e, g f8 e } \\ { r4 c( e d8 c } >> |
      << { a'1 } \\ { f2. d4)( } >> |
      << { g2 r } \\ { b,2) r } >> |
      << { r2 r4 cis'4( } \\ { r1 } >> |
      << { d2 g,) } \\ { r1 } >> |
      << { a2 g } \\ { a4( g8 f e d c b) } >> |
      << { a'4 c es d8 c~ } \\ { a,4( a' as2) } >> |
      << { c8 b a b a4 as } \\ { g2( c,~) } >> |
      << { g'2. as4 } \\ { c,1~ } >> |
      << { g'1 } \\ { c,1 } >> |
      \bar "|."
    }
  >>
}
